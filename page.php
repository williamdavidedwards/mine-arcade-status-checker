<?php
$ini_array = parse_ini_file("nl.ini");
?>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title><?php echo($ini_array['PAGETITLE']);?></title>
    <link rel="stylesheet" type="text/css" href="css/style.css?ver=1.3">
  </head>
  <body>
    <div id="container">
    <div style="width: 400px;">
		<div style="float: left; width: 37px;"><img src="https://minearcade.nl/images/logo.png" alt="Mine/Arcade logo" /></div>
		<div style="float: left; width: 370px;"><p><?php echo($ini_array['MADESC']);?></p></div>
		<div style="float: left; width: 370px;"><a href="https://minearcade.nl"><?php echo($ini_array['MORE_INFO']);?></a></div>
		<div style="clear: both;"></div>
	</div>

    <br /><br />
      <form method="post" action="check.php">
  <?php echo($ini_array['IS']);?>
  <input type="text" name="domain" id="domain_input" placeholder="127.0.0.1" autofocus />
  :
  <input type="text" name="port" id="port_input" placeholder="25565" />
  <?php echo($ini_array['DESC']);?>
  <input type="submit" value="<?php echo($ini_array['SUBMITBUTTON']);?>" />
</form>
    </div>
  </body>
</html>