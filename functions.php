<?php
if (count(get_included_files()) == 1)
{
	die("Direct access is not allowed");
}

class ServerCheck 
{
	function serverStatus($ip, $port)
	{
		$connectionStream = @fsockopen($ip, $port, $errno, $errstr, 2);
		
		if($connectionStream >= 1)
		{
			return 'Server is <font color="green">online</font>';
		}
		else
		{
			return 'Server is <font color="red">offline</font>';
		}
	}
}
?>