<?php
error_reporting(-1);
ini_set('display_errors', 1);

include('page.php');

include_once('functions.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$domain = $_POST['domain'];
	$domainval = '/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/';
	
	if (empty($_POST['port']) and empty($_POST['domain']))
	{
		die("Domein en IP mogen niet leeg zijn");
	}
	
	if (empty($_POST['port']))
	{
		$port = '25565';
	}
	else
	{
		$port = $_POST['port'];
	}
	
	if (empty($_POST['domain']))
	{
		die("Domein of IP mag niet leeg zijn");
	}
	else
	{
		$domain = $_POST['domain'];
	}
	
	if (!preg_match($domainval, $domain))
	{
		$serverCheck = new ServerCheck();
		print_r($serverCheck->serverStatus($domain, $port));
	}
	else if (preg_match($domainval, $domain))
	{
		$ipaddr = gethostbyname($domain);
		
		$serverCheck = new ServerCheck();
		print_r($serverCheck->serverStatus($ipaddr, $port));
	}

	die();
}
elseif (!($_SERVER['REQUEST_METHOD'] == 'POST')) {
	echo 'Deze tool is <a href="https://bitbucket.org/williamdavidedwards/mine-arcade-status-checker/src/" target="_blank">open source</a>!';
}
?>